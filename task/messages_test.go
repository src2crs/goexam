package task

import "fmt"

func ExampleMessages() {
	m1 := Messages{
		okString:       "ok",
		errorString:    "error",
		expectedString: "expected",
		actualString:   "actual",
	}
	m2 := Messages{
		okString:       "OK",
		errorString:    "ERROR",
		expectedString: "EXPECTED",
		actualString:   "ACTUAL",
	}
	taskName1 := "task1"
	fmt.Println(m1.OkMsg(taskName1))
	fmt.Println(m1.ErrorMsg(taskName1, 42, 43))
	fmt.Println(m2.OkMsg(taskName1))
	fmt.Println(m2.ErrorMsg(taskName1, 42, 43))

	// Output:
	// [task1] ok
	// [task1] error, expected: 42, actual: 43
	// [task1] OK
	// [task1] ERROR, EXPECTED: 42, ACTUAL: 43
}
