package task

// Container for components of messages.
// Used to specify the actual words used in a message to say whether an assertion
// has failed or succeeded, how to indicate expected and actual values etc.
type Messages struct {
	okString       string
	errorString    string
	expectedString string
	actualString   string
}

// Returns the ok message for a given task name.
func (m *Messages) OkMsg(taskName string) string {
	return messageHeader(taskName, m.okString)
}

// Returns the beginning of an error message for a given task name.
func (m *Messages) ErrorMsgHeader(taskName string) string {
	return messageHeader(taskName, m.errorString)
}

// Returns the actual content of an error message for a given task name.
func (m *Messages) ErrorMsgContent(expected, actual any) string {
	return errorMessageContent(m.expectedString, m.actualString, expected, actual)
}

// Returns the complete error message for a given task name.
func (m *Messages) ErrorMsg(taskName string, expected, actual any) string {
	return m.ErrorMsgHeader(taskName) + ", " + m.ErrorMsgContent(expected, actual)
}
