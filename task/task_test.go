package task

import (
	"fmt"
	"testing"
)

func ExampleTask_ExpectEqual() {
	t1 := NewTaskWithMsgs("TestTask 1", "ok", "error", "expected", "actual")

	s1, b1 := t1.ExpectEqual(42, fortytwo())
	fmt.Println(s1)
	fmt.Println(b1)

	s2, b2 := t1.ExpectEqual(43, fortytwo())
	fmt.Println(s2)
	fmt.Println(b2)

	s3, b3 := t1.ExpectEqual("Hello", sayHello())
	fmt.Println(s3)
	fmt.Println(b3)

	s4, b4 := t1.ExpectEqual("Hi", sayHello())
	fmt.Println(s4)
	fmt.Println(b4)

	// Output:
	// [TestTask 1] ok
	// true
	// [TestTask 1] error, expected: 43, actual: 42
	// false
	// [TestTask 1] ok
	// true
	// [TestTask 1] error, expected: Hi, actual: Hello
	// false
}

func TestTask_AssertEqual(t *testing.T) {
	t1 := NewTaskWithMsgs("TestTask 1", "ok", "error", "expected", "actual")
	t1.AssertEqual(t, 42, fortytwo())
	t1.AssertEqual(t, "Hello", sayHello())
}
