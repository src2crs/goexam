package task

import "testing"

// Container type for a task.
type Task struct {
	name string
	msgs Messages
}

// Returns the ok message for this task.
// Contains the message header along with the ok string.
func (t *Task) OkMsg() string {
	return t.msgs.OkMsg(t.name)
}

// This task's error message.
// Contains the message header along with the error content.
func (t *Task) ErrorMsg(expected, value any) string {
	return t.msgs.ErrorMsg(t.name, expected, value)
}

// Expects two values for expected and actual value that should be compared.
// Returns the result message and a bool indicating the result.
func (t *Task) ExpectEqual(expected, value any) (string, bool) {
	if expected == value {
		return t.OkMsg(), true
	}
	return t.ErrorMsg(expected, value), false
}

// Checks whether the given values are equal and produces a test failure if not.
func (t *Task) AssertEqual(test *testing.T, expected, value any) {
	msg, success := t.ExpectEqual(expected, value)
	if !success {
		test.Error(msg)
	}
}

// Creates a new Task with the given name and the given values for the Message strings.
func NewTaskWithMsgs(taskName, okstring, errorstring, expectedstring, actualstring string) Task {
	messages := Messages{
		okString:       okstring,
		errorString:    errorstring,
		expectedString: expectedstring,
		actualString:   actualstring,
	}
	return Task{name: taskName, msgs: messages}
}
