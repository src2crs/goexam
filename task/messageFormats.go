package task

import "fmt"

const (
	messageHeaderFormatString       = "[%v] %v"
	errorMessageContentFormatString = "%v: %v, %v: %v"
)

// Returns a message header consisting of the given task name
// and a string indicating success or failure.
func messageHeader(taskName, success string) string {
	return fmt.Sprintf(messageHeaderFormatString, taskName, success)
}

// Returns the content of an error message indication expected and actual values.
func errorMessageContent(expectedString, actualString string, expectedValue, actualValue any) string {
	return fmt.Sprintf(errorMessageContentFormatString, expectedString, expectedValue, actualString, actualValue)
}
